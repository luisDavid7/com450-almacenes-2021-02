const urlRoute = 'http://localhost:8080/api/';
function initProductEntry() {
  getCodes().then((response) => {
    response = JSON.parse(response);
    $("#input-code").autocomplete({
      source: response
    });
  })
}

function getCodes() {
  return new Promise((resolved) => {
    const http = new XMLHttpRequest();
    http.open("GET", urlRoute+"producto/codigosListado");
    http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    http.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        let data = this.responseText;
        resolved(data);
      }
    };
    http.send();
  });
}

function addProductTable(option) {
  let code = document.querySelector("#input-code").value;
  const http = new XMLHttpRequest();
  let data = { codigo: code };   
  http.open("POST", urlRoute+"producto/codigo");
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let data = this.responseText;
      data = JSON.parse(data);
      let htmlOption = option==1 ? '' : 
      `<td><input type="text" size="4" onchange="changePrice(${data.id})"; id="precio-venta-${data.id}" name="precio-venta" class="price-sale" value="${data.precio}"/> Bs</td>
      <td id="price-${data.id}"; class="prices">${data.precio} Bs</td>`;
      let htmlQuantity = option==1 ? '`<td><input type="number" id="quantity-${data.id}" name="cantidad" class="quantity" value="1"></td>`' :
       `<td><input type="number" id="quantity-${data.id}" name="cantidad" onchange="changeQuantity(${data.id})"; class="quantity" value="1"></td>`;
      let html = `<tr id="tr-${data.id}">
        <th scope="row"><strong class="row-number">${data.id}</strong></th>
        <td>${data.codigo} <input type="hidden" name="idProducto" value="${data.id}"/></td>
        <td>${data.nombre}</td>
        <td>${data.precio} Bs</td>
        ${htmlQuantity}
        ${htmlOption}
        <td class="text-center"><button class="btn btn-sm bg-danger text-white" onclick="removeProductTable(${data.id})"><i class="fas fa-times"></i></button></td>
      </tr>`;
      $("#tbody").append(html);
      calculateCost();
    }
    numerateTableStore();
  };
  http.send(JSON.stringify(data));
  document.querySelector("#input-code").value = "";
}

function changeQuantity(id) {
  if(document.querySelector('#quantity-'+id).value <= 0) {
    alert('La cantidad debe ser mayor a cero.');
    document.querySelector('#quantity-'+id).value = 1;
  }
  calculateCost();
}

function changePrice(id) {
  if(isNaN(document.querySelector('#precio-venta-'+id).value) && document.querySelector('#precio-venta-'+id).value <= 0) {
    alert('La cantidad debe ser mayor a cero y de valor numérico.');
    document.querySelector('#precio-venta-'+id).value = 1;
  }
  calculateCost();
}

function calculateCost() {
  let total = 0;
  let op = 0;
  let priceSale = document.getElementsByClassName('price-sale');
  let quantity = document.getElementsByClassName('quantity');
  let prices = document.getElementsByClassName('prices');
  for(let i = 0; i < priceSale.length; i++) {
    op = quantity[i].value*priceSale[i].value;
    total += op;
    prices[i].innerHTML =  op +' Bs';
  }
  document.querySelector('#tfoot-cost').innerHTML = total + ' Bs';
}

function numerateTableStore() {
  let rows = document.getElementsByClassName('row-number');
  for(let i = 0; rows.length; i++) {
    rows[i].innerHTML = i+1;
  }
}

function removeProductTable(id) {
  $("#tr-" + id).remove();
}

function sendFormEntryProduct(option) {
  if(document.querySelector('#tbody').rows.length > 0) {
    let formData = new FormData(document.querySelector("#form"));
    let codigo = generateCode();
    var datos = [];
    if(option == 1) {
      let idProducts = formData.getAll("idProducto");
      let cantidad = formData.getAll("cantidad");
      for (var i = 0; i < formData.getAll("idProducto").length; i++) {
        datos.push({
          idProducto: parseInt(idProducts[i]),
          cantidad: parseInt(cantidad[i]),
          operacion: "entrada",
          codigo: codigo,
          usuario : JSON.parse(localStorage.getItem('user'))
        });
      }
    } else {
      let idProducts = formData.getAll("idProducto");
      let cantidad = formData.getAll("cantidad");
      let precioVenta = formData.getAll("precio-venta");
      for (var i = 0; i < formData.getAll("idProducto").length; i++) {
        datos.push({
          idProducto: parseInt(idProducts[i]),
          cantidad: parseInt(cantidad[i]),
          operacion: "salida",
          precioventa: parseFloat(precioVenta[i]),
          codigo: codigo,
          usuario : JSON.parse(localStorage.getItem('user'))
        });
      }
    }
    let url = urlRoute+"almacen/addLista";
    fetch(url, {
      method: 'POST',
      body: JSON.stringify(datos),
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => {
      if(res.status==200) {
        alert('Guardado corréctamente.');
        $('#tbody').html('');
        $('#tfoot-cost').html('0 Bs');
      } else {
        alert('Ocurrió un error en el guardado.')
      }
    })
    .catch(error => console.error('Error:', error))
  } else {
    alert('Error, debe agregar productos.');
  }
}

function generateCode() {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < 8; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}


function initProducts() {
  $('#tbody').html('');
  const http = new XMLHttpRequest();
  http.open("GET", urlRoute+"producto/todosProductos");
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let data = this.responseText;
      data = JSON.parse(data);
      let html = '';
      let button = '';
      data.forEach((element, index) => {
        button = element.estado? `<button class="btn btn-danger btn-sm" onclick="disable(${element.id});">Deshabilitar</button>`:
        `<button class="btn btn-success btn-sm" onclick="enable(${element.id});">Habilitar</button>`;
        html += `<tr>
          <td>${index+1}</td>
          <td>${element.codigo}</td>
          <td>${element.nombre}</td>
          <td>${element.precio} Bs</td>
          <td>${element.cantidad} unidades</td>
          <td>
            <button class="btn btn-warning btn-sm" onclick="edit(${element.id});"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</button>
            ${button}
          </td>
        </tr>`;
      });
      $('#tbody').html(html);
    }
  };
  http.send();
}

function flagForm(flag) {
  if(flag) {
    document.querySelector('#div-form').style.display = 'block';
    document.querySelector('#div-table').style.display = 'none';
    document.querySelector('#btn-new').style.display = 'none';
  } else {
    document.querySelector('#div-form').style.display = 'none';
    document.querySelector('#div-table').style.display = 'block';
    document.querySelector('#btn-new').style.display = 'block';
  }
}

function clearFormProducto() {
  document.querySelector('#idproducto').value = '';
  document.querySelector('#codigo').value = '';
  document.querySelector('#producto').value = '';
  document.querySelector('#cantidad').value = '';
  document.querySelector('#precio').value = '';
}

function edit(id) {
  const http = new XMLHttpRequest();
  http.open("GET", urlRoute+"producto/byId/"+id);
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let data = this.responseText;
      data = JSON.parse(data);
      document.querySelector('#idproducto').value = id;
      document.querySelector('#codigo').value = data.codigo;
      document.querySelector('#producto').value = data.nombre;
      document.querySelector('#precio').value = data.precio;
      document.querySelector('#cantidad').value = data.cantidad;
      flagForm(true);
    }
  };
  http.send();
}

function enable(id) {
  if(confirm('¿Está segur@ de habilitar este producto?')) {
    data = { id : id };
    const http = new XMLHttpRequest();
      http.open("POST", urlRoute+"producto/habilitar");
      http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      http.onreadystatechange = function () {
        if (this.readyState == 4) {
          if(this.status == 200) {
            initProducts();
            flagForm(false);
            alert('Producto habilitado corréctamente.');
          } else {
            alert('Ocurrió un error.');
          }
        }
      };
      http.send(JSON.stringify(data));
  }
}

function disable(id) {
  if(confirm('¿Está segur@ de deshabilitar este producto?')) {
    data = { id : id };
  const http = new XMLHttpRequest();
    http.open("POST", urlRoute+"producto/deshabilitar");
    http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    http.onreadystatechange = function () {
      if (this.readyState == 4) {
        if(this.status == 200) {
          initProducts();
          flagForm(false);
          alert('Producto deshabilitado corréctamente.');
        } else {
          alert('Ocurrió un error.');
        }
      }
    };
    http.send(JSON.stringify(data));
  };
}

function saveProduct() {
  let idInput = document.querySelector('#idproducto').value.trim();
  if(idInput != '') {
    data = { 
      id : idInput, 
      codigo : document.querySelector('#codigo').value, 
      nombre : document.querySelector('#producto').value, 
      cantidad : document.querySelector('#cantidad').value, 
      precio : document.querySelector('#precio').value
    };
  } else {
    data = { 
      codigo : document.querySelector('#codigo').value, 
      nombre : document.querySelector('#producto').value, 
      cantidad : document.querySelector('#cantidad').value, 
      precio : document.querySelector('#precio').value,
      estado : 1
    };
  }
  const http = new XMLHttpRequest();
  let endRoute = idInput== '' ? 'add': 'editar';
  http.open("POST", urlRoute+"producto/"+endRoute);
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.onreadystatechange = function () {
    if (this.readyState == 4) {
      if(this.status == 200) {
        let data = this.responseText;
        alert(idInput=='' ? "Guardado corrétamente" : "Actualizado corrétamente");
        initProducts();
        flagForm(false);
      } else {
        alert(idInput=='' ? "Ocurrió un error al guardar el producto" : "Ocurrió un error en la actualización.");
      }
    }
  };
  http.send(JSON.stringify(data));
}


function initUsers() {
  $('#tbody').html('');
  const http = new XMLHttpRequest();
  http.open("GET", urlRoute+"usuario/todos");
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let data = this.responseText;
      data = JSON.parse(data);
      let html = '';
      let button = '';
      data.forEach((element, index) => {
        button = element.estado? `<button class="btn btn-danger btn-sm" onclick="disable(${element.id});">Deshabilitar</button>`:
        `<button class="btn btn-success btn-sm" onclick="enable(${element.id});">Habilitar</button>`;
        html += `<tr>
          <td>${index+1}</td>
          <td>${element.correo}</td>
          <td>${element.nombre}</td>
          <td>${element.apellidos}</td>
          <td>${element.usuario}</td>
          <td>${element.tipo_usuario.toUpperCase()}</td>
          <td>${button}</td>
        </tr>`;
      });
      $('#tbody').html(html);
    }
  };
  http.send();
}

function cleanFormUser() {
  document.querySelector('#nombre').value = '';
  document.querySelector('#apellidos').value = '';
  document.querySelector('#usuario').value = '';
  document.querySelector('#contrasenia').value = '';
  document.querySelector('#email').value = '';
}

function enable(id) {
  if(confirm('¿Está segur@ de habilitar este usuario?')) {
    data = { id : id };
    const http = new XMLHttpRequest();
      http.open("POST", urlRoute+"usuario/habilitar");
      http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      http.onreadystatechange = function () {
        if (this.readyState == 4) {
          if(this.status == 200) {
            initUsers();
            flagForm(false);
            alert('Usuario habilitado corréctamente.');
          } else {
            alert('Ocurrió un error.');
          }
        }
      };
      http.send(JSON.stringify(data));
  }
}

function disable(id) {
  if(confirm('¿Está segur@ de deshabilitar este usuario?')) {
    data = { id : id };
  const http = new XMLHttpRequest();
    http.open("POST", urlRoute+"usuario/deshabilitar");
    http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    http.onreadystatechange = function () {
      if (this.readyState == 4) {
        if(this.status == 200) {
          initUsers();
          flagForm(false);
          alert('Usuario deshabilitado corréctamente.');
        } else {
          alert('Ocurrió un error.');
        }
      }
    };
    http.send(JSON.stringify(data));
  };
}

function saveUser() {
  json = { 
    nombre : document.querySelector('#nombre').value, 
    apellidos : document.querySelector('#apellidos').value, 
    usuario : document.querySelector('#usuario').value, 
    contrasena : document.querySelector('#contrasenia').value, 
    correo : document.querySelector('#email').value
  };
  const http = new XMLHttpRequest();
  http.open("POST", urlRoute+"usuario/add");
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.onreadystatechange = function () {
    if (this.readyState == 4) {
      if(this.status == 200) {
        let data = this.responseText;
        alert("Guardado corrétamente");
        initUsers();
        flagForm(false);
      } else {
        alert("Ocurrió un error al guardar el usuario");
      }
    }
  };
  http.send(JSON.stringify(json));
}

function initReports() {
  let fecha = document.querySelector('#fecha').value;
  getDataInput(fecha);
  getDataOutput(fecha);
}
function getDataInput(fecha) {
  $('#tbody-input').html('');
  let json = { 
    fecha : fecha, 
    operacion : "entrada" 
  };
  const http = new XMLHttpRequest();
  http.open("POST", urlRoute+"almacen/almacenCodigosPrecio");
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let data = this.responseText;
      data = JSON.parse(data);
      let html = '';
      data.forEach((element, index) => {
        html += `<tr>
          <td>${index+1}</td>
          <td>${element.fecha}</td>
          <td><button class="btn btn-sm btn-info" onclick="showModal('${element.codigo}');">Mostrar</button></td>
        </tr>`;
      });
      $('#tbody-input').html(html);
    }
  };
  http.send(JSON.stringify(json));
}
function getDataOutput(fecha) {
  $('#tbody-output').html('');
  let json = { 
    fecha : fecha, 
    operacion : "salida" 
  };
  const http = new XMLHttpRequest();
  http.open("POST", urlRoute+"almacen/almacenCodigosPrecio");
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let data = this.responseText;
      data = JSON.parse(data);
      let html = '';
      data.forEach((element, index) => {
        html += `<tr>
          <td>${index+1}</td>
          <td>${element.fecha}</td>
          <td>${element.precioVentaTotal}</td>
          <td><button class="btn btn-sm btn-info" onclick="showModal('${element.codigo}');">Mostrar</button></td>
        </tr>`;
      });
      $('#tbody-output').html(html);
    }
  };
  http.send(JSON.stringify(json));
}

function showModal(code) {
  $('#tbody-modal').html('');
  let json = { 
    codigo : code,
    id : 0
  };
  const http = new XMLHttpRequest();
  http.open("POST", urlRoute+"almacen/byCodigo");
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let data = this.responseText;
      data = JSON.parse(data);
      let html = '';
      data.forEach((element, index) => {
        html += `<tr>
          <td>${index+1}</td>
          <td>${element.producto.nombre}</td>
          <td>${element.cantidad}</td>
        </tr>`;
      });
      $('#tbody-modal').html(html);
    }
  };
  http.send(JSON.stringify(json));
  $('#modal').modal('show');
}

function login() {
  let user = document.querySelector('#user').value;
  let password = document.querySelector('#password').value;
  if(user.trim() != '' && password.trim() !='') {
    let json = { 
      usuario : user,
      contrasena : password
    };
    const http = new XMLHttpRequest();
    http.open("POST", urlRoute+"usuario/loguearse");
    http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    http.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        let data = this.responseText;
        data = JSON.parse(data);
        if(data.id != null) {
          localStorage.setItem("user", JSON.stringify(data));
          document.location.href = 'producto.html';
        } else {
          alert('Usuario y/ó contraseña incorrectos');
          document.querySelector('#user').value = '';
          document.querySelector('#password').value = '';
        }
      }
    };
    http.send(JSON.stringify(json));
  } else {
    alert('Complete los campos');
  }
}

function verifyLogin() {
  if(localStorage.getItem('user') != null && localStorage.getItem('user') != '') {
    document.location.href = 'producto.html';
  }
}

function isLogin() {
  if(localStorage.getItem('user') == null || localStorage.getItem('user') == '') {
    document.location.href = 'index.html';
  }
}

function exit() {
  if(localStorage.getItem('user') != null && localStorage.getItem('user') != '') {
    localStorage.removeItem('user');
    document.location.href = 'index.html';
  }
}
