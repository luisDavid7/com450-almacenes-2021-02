--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.23
-- Dumped by pg_dump version 9.6.23

-- Started on 2022-01-19 09:14:19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2157 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 16396)
-- Name: almacen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.almacen (
    id bigint NOT NULL,
    cantidad integer,
    fecha date,
    operacion character varying(255),
    precioventa double precision,
    producto_id bigint,
    usuario_id bigint,
    codigo character varying(255)
);


ALTER TABLE public.almacen OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16394)
-- Name: almacen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.almacen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.almacen_id_seq OWNER TO postgres;

--
-- TOC entry 2158 (class 0 OID 0)
-- Dependencies: 185
-- Name: almacen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.almacen_id_seq OWNED BY public.almacen.id;


--
-- TOC entry 188 (class 1259 OID 16404)
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.producto (
    id bigint NOT NULL,
    codigo character varying(255),
    fecha_registro timestamp with time zone,
    nombre character varying(255),
    precio double precision,
    estado boolean,
    cantidad integer
);


ALTER TABLE public.producto OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 16402)
-- Name: producto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.producto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.producto_id_seq OWNER TO postgres;

--
-- TOC entry 2159 (class 0 OID 0)
-- Dependencies: 187
-- Name: producto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.producto_id_seq OWNED BY public.producto.id;


--
-- TOC entry 190 (class 1259 OID 16415)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id bigint NOT NULL,
    apellidos character varying(255),
    contrasena character varying(255),
    correo character varying(255),
    nombre character varying(255),
    tipo_usuario character varying(255),
    usuario character varying(255),
    estado boolean
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 16413)
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2160 (class 0 OID 0)
-- Dependencies: 189
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;


--
-- TOC entry 2016 (class 2604 OID 16399)
-- Name: almacen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.almacen ALTER COLUMN id SET DEFAULT nextval('public.almacen_id_seq'::regclass);


--
-- TOC entry 2017 (class 2604 OID 16407)
-- Name: producto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto ALTER COLUMN id SET DEFAULT nextval('public.producto_id_seq'::regclass);


--
-- TOC entry 2018 (class 2604 OID 16418)
-- Name: usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);


--
-- TOC entry 2145 (class 0 OID 16396)
-- Dependencies: 186
-- Data for Name: almacen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.almacen (id, cantidad, fecha, operacion, precioventa, producto_id, usuario_id, codigo) FROM stdin;
24	10	2021-12-02	entrada	\N	1	1	y8iQhBba
25	10	2021-12-02	entrada	\N	4	1	y8iQhBba
26	10	2021-12-02	entrada	\N	7	1	pXRpB3lC
27	10	2021-12-02	entrada	\N	5	1	pXRpB3lC
28	4	2021-12-02	salida	1.5	1	1	sqeWpOsL
29	2	2021-12-02	salida	25.5	4	1	sqeWpOsL
30	10	2021-12-03	entrada	\N	2	1	KEFtdPyS
31	20	2021-12-03	entrada	\N	4	1	vf9MA5lI
32	15	2021-12-03	entrada	\N	7	1	bULsdFnS
33	10	2021-12-03	entrada	\N	9	1	bULsdFnS
34	5	2021-12-03	salida	5	6	1	tjJ9pqkJ
35	1	2021-12-03	salida	1.5	2	1	tjJ9pqkJ
\.


--
-- TOC entry 2161 (class 0 OID 0)
-- Dependencies: 185
-- Name: almacen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.almacen_id_seq', 35, true);


--
-- TOC entry 2147 (class 0 OID 16404)
-- Dependencies: 188
-- Data for Name: producto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.producto (id, codigo, fecha_registro, nombre, precio, estado, cantidad) FROM stdin;
8	C-008	2021-11-19 08:14:57.177-04	Pizarra	150	t	5
5	C-004	2021-11-18 20:25:21.232-04	Colores	15	t	30
1	C-001	2021-11-11 22:33:10.232535-04	Lapiz	1.5	t	66
4	C-003	2021-11-18 20:23:02.53-04	Hojas archivador	25.5	t	65
7	C-006	2021-11-18 22:27:18.19-04	Estuchera	50	t	51
9	C-009	2021-11-19 08:23:03.062-04	Boligrafo	1.5	t	20
6	C-005	2021-11-19 08:23:03.062-04	Marcador de agua negro	5	f	45
2	C-002	2021-11-11 22:33:10.232535-04	Borrador	1.5	t	59
\.


--
-- TOC entry 2162 (class 0 OID 0)
-- Dependencies: 187
-- Name: producto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.producto_id_seq', 9, true);


--
-- TOC entry 2149 (class 0 OID 16415)
-- Dependencies: 190
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id, apellidos, contrasena, correo, nombre, tipo_usuario, usuario, estado) FROM stdin;
2	Gomes	123456	gilber@gmail.com	Gilber	empleado	gilber	t
3	contreras	123456	ariel@gmail.com	ariel	empleado	ariel	t
1	Mostacedo	123456	usuario@gmail.com	Raul	administrador	admin	t
\.


--
-- TOC entry 2163 (class 0 OID 0)
-- Dependencies: 189
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_seq', 3, true);


--
-- TOC entry 2020 (class 2606 OID 16401)
-- Name: almacen almacen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.almacen
    ADD CONSTRAINT almacen_pkey PRIMARY KEY (id);


--
-- TOC entry 2022 (class 2606 OID 16412)
-- Name: producto producto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_pkey PRIMARY KEY (id);


--
-- TOC entry 2024 (class 2606 OID 16423)
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2026 (class 2606 OID 16429)
-- Name: almacen fk6o8r2oq52gq7fl759y2irqhgi; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.almacen
    ADD CONSTRAINT fk6o8r2oq52gq7fl759y2irqhgi FOREIGN KEY (usuario_id) REFERENCES public.usuario(id);


--
-- TOC entry 2025 (class 2606 OID 16424)
-- Name: almacen fkfx56wiv7yibui2y6268pr3s2o; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.almacen
    ADD CONSTRAINT fkfx56wiv7yibui2y6268pr3s2o FOREIGN KEY (producto_id) REFERENCES public.producto(id);


-- Completed on 2022-01-19 09:14:19

--
-- PostgreSQL database dump complete
--

