package com.example.spring.calidad.controller;

import com.example.spring.calidad.CodigoRecibir;
import com.example.spring.calidad.almacenesGrupos;
import com.example.spring.calidad.modelo.Almacen;
import com.example.spring.calidad.modelo.Producto;
import com.example.spring.calidad.repositorio.RepoAlmacen;
import com.example.spring.calidad.repositorio.RepoProducto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/almacen")
public class ControllerAlmacen {
    RepoAlmacen repoAlmacen;
    RepoProducto repoProducto;
    public ControllerAlmacen(RepoAlmacen repoAlmacen, RepoProducto repoProducto){
        this.repoAlmacen = repoAlmacen;
        this.repoProducto = repoProducto;
    }
    @PostMapping("/add")
    public void agregar(@RequestBody Almacen almacen){
        Producto productoNuevo = repoProducto.findById(almacen.getIdProducto()).orElseThrow(IllegalStateException::new);
        almacen.setProducto(productoNuevo);
        repoAlmacen.save(almacen);
    }
    @PostMapping("/addLista")
    public void agregarListas(@RequestBody List<Almacen> almacen){
        almacen.forEach(data->{
            Producto productoNuevo = repoProducto.findById(data.getIdProducto()).orElseThrow(IllegalStateException::new);
            if(data.getOperacion().equals("entrada")){
                productoNuevo.setCantidad(productoNuevo.getCantidad()+data.getCantidad());
            }
            if(data.getOperacion().equals("salida")){
                productoNuevo.setCantidad(productoNuevo.getCantidad()-data.getCantidad());
            }
            Producto cambiadoProducto= repoProducto.save(productoNuevo);
            data.setProducto(cambiadoProducto);

        });
        repoAlmacen.saveAll(almacen);
    }
    @GetMapping("/showDates")
    public ResponseEntity<List<Almacen>> porFecha(){
        List<Almacen> fechas = repoAlmacen.findAllByOrderByFechaDesc();
        return new ResponseEntity<>(fechas, HttpStatus.OK);
    }
    @PostMapping("/almacenCodigosPrecio")
    public ResponseEntity<List<Almacen>> grupos(@RequestBody almacenesGrupos almacenesGrupos){
        List<Almacen> fechas = repoAlmacen.grupos(almacenesGrupos.getOperacion(),almacenesGrupos.getFecha());
        return new ResponseEntity<>(fechas, HttpStatus.OK);
    }
    @PostMapping("/byCodigo")
    public ResponseEntity<List<Almacen>> grupos(@RequestBody CodigoRecibir codigoRecibir){
        List<Almacen> codigos = repoAlmacen.findByCodigo(codigoRecibir.getCodigo());
        return new ResponseEntity<>(codigos, HttpStatus.OK);
    }
}
