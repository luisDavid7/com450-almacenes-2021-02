package com.example.spring.calidad.controller;

import com.example.spring.calidad.CodigoRecibir;
import com.example.spring.calidad.LoginUsuario;
import com.example.spring.calidad.modelo.Producto;
import com.example.spring.calidad.modelo.Usuario;
import com.example.spring.calidad.repositorio.RepoUsuario;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/usuario")
public class ControllerUsuario {
    RepoUsuario repoUsuario;
    public ControllerUsuario(RepoUsuario repoUsuario){
        this.repoUsuario = repoUsuario;
    }
    @GetMapping("/todos")
    public ResponseEntity<List<Usuario>> TODOS(){
        List<Usuario> usuario = repoUsuario.findAll();
        return new ResponseEntity<>(usuario, HttpStatus.OK);
    }
    @PostMapping("/add")
    public void agregar(@RequestBody Usuario usuario){
        usuario.setEstado(true);
        if(Objects.isNull(usuario.getTipo_usuario()) ){
            usuario.setTipo_usuario("empleado");
        }
        repoUsuario.save(usuario);

    }
    @PostMapping("/habilitar")
    public ResponseEntity<CodigoRecibir> habilitar(@RequestBody CodigoRecibir codigoRecibir){
        Long num = Long.valueOf(codigoRecibir.getId());
        Usuario producto =repoUsuario.findById(num).orElseThrow(IllegalStateException::new);
        producto.setEstado(true);
        repoUsuario.save(producto);
        CodigoRecibir codigoRecibir1 = new CodigoRecibir();
        codigoRecibir1.setCodigo("exitoso");
        return new ResponseEntity<>(codigoRecibir1, HttpStatus.OK);
    }
    @PostMapping("/deshabilitar")
    public ResponseEntity<CodigoRecibir> deshabilitar(@RequestBody CodigoRecibir codigoRecibir){

        Long num = Long.valueOf(codigoRecibir.getId());
        Usuario producto =repoUsuario.findById(num).orElseThrow(IllegalStateException::new);
        producto.setEstado(false);
        repoUsuario.save(producto);
        CodigoRecibir codigoRecibir1 = new CodigoRecibir();
        codigoRecibir1.setCodigo("exitoso");
        return new ResponseEntity<>(codigoRecibir1, HttpStatus.OK);
    }
    @PostMapping("/loguearse")
    public ResponseEntity<Usuario> usuario(@RequestBody LoginUsuario loginUsuario){
        Usuario usuario = this.repoUsuario.findByUsuarioAndContrasena(loginUsuario.getUsuario(),loginUsuario.getContrasena());
        if(!Objects.isNull(usuario)){
            return new ResponseEntity<>(usuario,HttpStatus.OK);
        }else{
            Usuario usuario2 = new Usuario();
            usuario2.setUsuario("error");
            return new ResponseEntity<>(usuario2,HttpStatus.OK);
        }

    }
}
