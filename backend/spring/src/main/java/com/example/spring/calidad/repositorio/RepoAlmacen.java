package com.example.spring.calidad.repositorio;

import com.example.spring.calidad.modelo.Almacen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface RepoAlmacen extends JpaRepository<Almacen,Long> {
    List<Almacen> findAllByOrderByFechaDesc();
    @Query(value = "SELECT new com.example.spring.calidad.modelo.Almacen(a.fecha,a.codigo,SUM (a.precioventa)) from almacen a where a.operacion= :operacion and a.fecha= :fecha group by a.codigo,a.fecha")
    List<Almacen> grupos(@Param("operacion") String operacion, @Param("fecha") LocalDate fecha);
    List<Almacen> findByCodigo(String codigo);
}
