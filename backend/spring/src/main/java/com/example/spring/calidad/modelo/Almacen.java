package com.example.spring.calidad.modelo;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity(name = "almacen")
public class Almacen implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer cantidad;
    private Double precioventa;
    private String operacion;
    private String codigo;
    @Transient
    private Double precioVentaTotal;
    @Transient
    private Long idProducto;
    private LocalDate fecha;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "producto_id")
    Producto producto;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "usuario_id")
    Usuario usuario;
    @PrePersist
    public void fecha(){
        fecha = LocalDate.now();
    }
    public Almacen(LocalDate fecha, String codigo, Double precioVentaTotal){
        this.fecha = fecha;
        this.codigo = codigo;
        this.precioVentaTotal = precioVentaTotal;
    }

    public Almacen() {
    }
    
    
}
