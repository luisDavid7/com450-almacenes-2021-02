package com.example.spring.calidad.repositorio;

import com.example.spring.calidad.modelo.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoUsuario extends JpaRepository<Usuario,Long> {
    Usuario findByUsuarioAndContrasena(String Usuario, String Contrasena);
}
