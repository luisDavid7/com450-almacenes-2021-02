package com.example.spring.calidad.repositorio;

import com.example.spring.calidad.modelo.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RepoProducto extends JpaRepository<Producto,Long> {
    Optional<Producto> findById(Long id);
    Producto findByCodigo(String codigo);
}
