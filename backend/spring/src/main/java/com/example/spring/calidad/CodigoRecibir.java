package com.example.spring.calidad;

import java.io.Serializable;

public class CodigoRecibir implements Serializable {
     String codigo;
     Integer id;
    public String getCodigo(){
        return codigo;
    }
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
