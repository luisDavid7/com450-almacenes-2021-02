package com.example.spring.calidad.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity(name = "usuario")
public class Usuario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String usuario;
    private String contrasena;
    private String nombre;
    private String tipo_usuario;
    private String apellidos;
    private String correo;
    private Boolean estado;
    @OneToMany(mappedBy = "usuario")
            @JsonIgnore
    List<Almacen> almacens;
}