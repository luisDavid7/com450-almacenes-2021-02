package com.example.spring.calidad;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class almacenesGrupos implements Serializable {
    private String operacion;
    private LocalDate fecha;

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
}
