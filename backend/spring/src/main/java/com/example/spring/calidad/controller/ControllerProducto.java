package com.example.spring.calidad.controller;

import com.example.spring.calidad.CodigoRecibir;
import com.example.spring.calidad.modelo.Producto;
import com.example.spring.calidad.modelo.Usuario;
import com.example.spring.calidad.repositorio.RepoProducto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/producto")
public class ControllerProducto {
    RepoProducto repoProducto;
    public ControllerProducto(RepoProducto repoProducto){
        this.repoProducto = repoProducto;
    }

    @PostMapping("/add")
    public void agregar(@RequestBody Producto producto){

        repoProducto.save(producto);
    }
    @PostMapping("/editar")
    public void editar(@RequestBody Producto producto){
        Optional<Producto> producto1 = repoProducto.findById(producto.getId());
        producto.setEstado(producto1.get().getEstado());
        producto.setFechaRegistro(producto1.get().getFechaRegistro());
        repoProducto.save(producto);
    }
    @GetMapping("/byId/{id}")
    public ResponseEntity<Producto> byId(@PathVariable("id") Long id) {
        Optional<Producto> producto = repoProducto.findById(id);
        return new ResponseEntity<>(producto.get(), HttpStatus.OK);
    }
    @PostMapping("/codigo")
    public ResponseEntity<Producto> codigo(@RequestBody CodigoRecibir producto){
        Producto producto1 = repoProducto.findByCodigo(producto.getCodigo());
        return new ResponseEntity (producto1, HttpStatus.OK);
    }
    @GetMapping("/mostrar")
    public ResponseEntity<List<Producto>> mostrar(){
        return new ResponseEntity (repoProducto.findAll(), HttpStatus.OK);
    }
    @GetMapping("/codigosListado")
    public ResponseEntity<List<String>> codigosLista(){
        List<Producto> productoList = repoProducto.findAll();
        List<String> codigos = new ArrayList<>();
        for (Producto producto: productoList){
            codigos.add(producto.getCodigo());
        }
        return new ResponseEntity (codigos, HttpStatus.OK);
    }
    @GetMapping("/todosProductos")
    public ResponseEntity<List<Producto>> findAll(){
        List<Producto> productoList = repoProducto.findAll();
        return new ResponseEntity<>(productoList,HttpStatus.OK);
    }
    @PostMapping("/habilitar")
    public ResponseEntity<CodigoRecibir> habilitar(@RequestBody CodigoRecibir codigoRecibir){
        Long num = Long.valueOf(codigoRecibir.getId());
        Producto producto =repoProducto.findById(num).orElseThrow(IllegalStateException::new);
        producto.setEstado(true);
        repoProducto.save(producto);
        CodigoRecibir codigoRecibir1 = new CodigoRecibir();
        codigoRecibir1.setCodigo("exitoso");
        return new ResponseEntity<>(codigoRecibir1, HttpStatus.OK);
    }
    @PostMapping("/deshabilitar")
    public ResponseEntity<CodigoRecibir> deshabilitar(@RequestBody CodigoRecibir codigoRecibir){

        Long num = Long.valueOf(codigoRecibir.getId());
        Producto producto =repoProducto.findById(num).orElseThrow(IllegalStateException::new);
        producto.setEstado(false);
        repoProducto.save(producto);
        CodigoRecibir codigoRecibir1 = new CodigoRecibir();
        codigoRecibir1.setCodigo("exitoso");
        return new ResponseEntity<>(codigoRecibir1, HttpStatus.OK);
    }
}
